package com.uni.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootCouchBaseApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootCouchBaseApplication.class, args);
		System.out.println("Hello Spring Boot with CouchBase");
	}

}
