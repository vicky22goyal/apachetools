package com.uni.example.couchbase;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.couchbase.config.AbstractCouchbaseConfiguration;
import java.util.Arrays;
import java.util.List;

@Configuration
public class CouchbaseBucketConfig extends AbstractCouchbaseConfiguration {

	@Value("${couchbase.host}")
	protected String host;
	@Value("${couchbase.password}")
	protected String password;
	@Value("${couchbase.username}")
	protected String username;
	@Value("${couchbase.bucket}")
	protected String bucket;

	@Override
	public String getConnectionString() {
		return host;
	}

	@Override
	public String getUserName() {
		return username;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public String getBucketName() {
		return bucket;
	}
}