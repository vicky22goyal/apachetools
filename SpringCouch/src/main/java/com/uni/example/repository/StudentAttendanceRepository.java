package com.uni.example.repository;

import com.uni.example.model.StudentAttendance;
import org.springframework.data.couchbase.repository.CouchbaseRepository;

public interface StudentAttendanceRepository extends CouchbaseRepository<StudentAttendance, String> {

}
