package com.uni.example.repository;

import java.util.List;

import com.uni.example.model.Student;
import org.springframework.data.couchbase.core.query.View;
import org.springframework.data.couchbase.core.query.ViewIndexed;
import org.springframework.data.couchbase.repository.CouchbaseRepository;

@ViewIndexed(designDoc="student")
public interface StudentRepository extends CouchbaseRepository<Student, String> {
	@View(viewName="byFullName")
	public List<Student> findByFullName(String fullName);
	@View(viewName="countByFullName",reduce=true)
	public Integer getCountByFullName(String fullName);

}
