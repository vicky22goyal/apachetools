package com.example.kaf;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;

import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;
import java.util.concurrent.ExecutionException;

@Slf4j
public class ConsumerDemo {

    //final static Logger log = LoggerFactory.getLogger("ProducerDemo");
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        String serverConfig = "localhost:9092";
        String topic = "first";
        String groupId = "g1";
        //create consumer configs
        Properties prop = new Properties();
        prop.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, serverConfig);
        prop.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        prop.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        prop.setProperty(ConsumerConfig.GROUP_ID_CONFIG, groupId);
        //The reset policy is only used when a new consumer group is created.
        prop.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");

        //create the consumer
        KafkaConsumer<String,String> consumer = new KafkaConsumer<String, String>(prop);

        //subscribe the consumer
        consumer.subscribe(Arrays.asList(topic));

        //poll for new data
        //The above statement doesn't mean consumer will time out after 100 ms, it is the polling period.
        //Whatever data it captures in 100 ms is read into records collection.
        while(true) {
            ConsumerRecords<String,String> records =  consumer.poll(Duration.ofMillis(100));
            for(ConsumerRecord record : records) {
                log.info("Key : "+ record.key()+
                        " Value : "+record.value());
                log.info("Partition : "+ record.partition()+
                        " Offset : "+record.offset());
            }
        }

    }

}
