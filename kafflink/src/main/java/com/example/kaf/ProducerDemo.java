package com.example.kaf;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;
import java.util.Random;
import java.util.concurrent.ExecutionException;

@Slf4j
public class ProducerDemo {

    //final static Logger log = LoggerFactory.getLogger("ProducerDemo");
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        String serverConfig = "localhost:9092";
        //create producer properties
        Properties prop = new Properties();
        prop.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, serverConfig);
        prop.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        prop.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

        //create the producer
        KafkaProducer<String,String> producer = new KafkaProducer<String, String>(prop);

        for(int i =0;i<100;i++) {
            //create producer record
            String topic  = "first";
            String key = "id"+i;
            Random r = new Random();
            String val = "kafka"+r.nextInt(10);
            ProducerRecord<String, String> record = new ProducerRecord<String, String>(topic, key,val);

            //send data -asynchronous
            producer.send(record, new Callback() {
                @Override
                public void onCompletion(RecordMetadata recordMetadata, Exception e) {
                    if (e == null) {
                        log.info("Received new MetaData. \n" +
                                "Topic: " + recordMetadata.topic() + "\n" +
                                "Partition: " + recordMetadata.partition() + "\n" +
                                "Offset: " + recordMetadata.offset() + "\n" +
                                "Timestamp: " + recordMetadata.timestamp());
                    } else {
                        log.error("Error while producing:: ", e);
                    }
                }
            }).get(); // block the .send() to make it synchronous - don't do this in production
        }
        // flush data
        producer.flush();

        //flush and close data
        //producer.close();


    }

}
